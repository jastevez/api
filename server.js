//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
const requestjson = require('request-json');
var path = require('path');
const bodyParser = require('body-parser');
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ 
  extended: true
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


const urlmovimientos = 'https://api.mlab.com/api/1/databases/jestevez/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
const clientMovimientosMLab = requestjson.createClient(urlmovimientos);
clientMovimientosMLab.headers['Content-Type'] = 'application/json';

app.get('/', function(req, res){
  res.sendFile( path.join(__dirname + '/statics', 'index.html'));
});
app.get('/v1/movimientos', function(req, res){
  res.sendFile( path.join(__dirname + '/statics', 'movimientos.v1.json'));
});
app.get('/clientes/:idcliente/:nombrecliente', function(req, res){
  const idcliente = req.params['idcliente'];
  const nombrecliente = req.params['nombrecliente'];
  res.send(`número de cliente: ${idcliente}, nombre: ${nombrecliente}`);
});
var path = require('path');
app.post('/', function(req, res){
  res.send('Hemos recibido su petición post');
});
app.put('/', function(req, res){
  res.send('Hemos recibido su petición put cambiada');
});
app.delete('/', function(req, res){
  res.send('Hemos recibido su petición delete');
});

//MLab
app.get('/movimiento', function(req, res){
   clientMovimientosMLab.get('', function(err, resm, body){
    if(err)
      res.send('');
    else
      res.send(body);
   });
});
app.post('/movimiento', function(req, res){
  clientMovimientosMLab.post('', req.body, function(err, resm, body){
   if(err)
     res.send('');
   else
     res.send(body);
  });
});

app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

